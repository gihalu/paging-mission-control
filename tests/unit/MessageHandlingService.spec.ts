import { expect } from 'chai';
import { MessageHandlingService } from '@/services/MessageHandling/MessageHandling.service';
import { SatelliteComponent, SatelliteMessage } from '@/services/MessageHandling/MessageHandling.models';

const messageHandling = new MessageHandlingService();

describe('MessageHandlingService', () => {

  it('converts text from file to message objects', () => {
    const fileText = '20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT';
    const messages = messageHandling.getMessagesFromText(fileText);
    const expectedMessage: SatelliteMessage = {
      component: SatelliteComponent.Thermostat,
      satelliteId: 1001,
      timestamp: new Date('2018-01-01T23:01:05.001'),
      redHighLimit: 101,
      yellowHighLimit: 98,
      yellowLowLimit: 25,
      redLowLimit: 20,
      rawValue: 99.9
    };
    expect(messages).to.deep.equal([expectedMessage]);
  });

  it('flags messages that meet specified criteria', () => {
    const violations = messageHandling.getViolations([{
      timestamp: new Date("2018-01-02T04:01:38.001Z"),
      satelliteId: 1000,
      redHighLimit: 101,
      yellowHighLimit: 98,
      yellowLowLimit: 25,
      redLowLimit: 20,
      rawValue: 102.9,
      component: SatelliteComponent.Thermostat,
    }]);
    expect(violations).to.have.lengthOf(1);
  });

  it('does not flag messages that do meet specified criteria', () => {
    const violations = messageHandling.getViolations([{
      timestamp: new Date("2018-01-02T04:01:38.001Z"),
      satelliteId: 1000,
      redHighLimit: 101,
      yellowHighLimit: 98,
      yellowLowLimit: 25,
      redLowLimit: 20,
      rawValue: 100.8,
      component: SatelliteComponent.Thermostat,
    }]);
    expect(violations).to.have.lengthOf(0);
  });

  it ('returns alerts when messages meet alert criteria', () => {
    const alerts = messageHandling.getAlertMessages([{
      timestamp: new Date("2018-01-02T04:01:38.001Z"),
      satelliteId: 1000,
      redHighLimit: 101,
      yellowHighLimit: 98,
      yellowLowLimit: 25,
      redLowLimit: 20,
      rawValue: 102.9,
      component: SatelliteComponent.Thermostat,
      evaluatorKey: "highTemperature",
      groupKey: "TSTAT.1000"
    },{
      timestamp: new Date("2018-01-02T04:02:38.001Z"),
      satelliteId: 1000,
      redHighLimit: 101,
      yellowHighLimit: 98,
      yellowLowLimit: 25,
      redLowLimit: 20,
      rawValue: 102.9,
      component: SatelliteComponent.Thermostat,
      evaluatorKey: "highTemperature",
      groupKey: "TSTAT.1000"
    },{
      timestamp: new Date("2018-01-02T04:03:38.001Z"),
      satelliteId: 1000,
      redHighLimit: 101,
      yellowHighLimit: 98,
      yellowLowLimit: 25,
      redLowLimit: 20,
      rawValue: 102.9,
      component: SatelliteComponent.Thermostat,
      evaluatorKey: "highTemperature",
      groupKey: "TSTAT.1000"
    }])
    expect(alerts).to.have.lengthOf(1);
  });

  it ('does not return alerts when messages do not meet alert criteria', () => {
    const alerts = messageHandling.getAlertMessages([{
      timestamp: new Date("2018-01-02T04:01:38.001Z"),
      satelliteId: 1000,
      redHighLimit: 101,
      yellowHighLimit: 98,
      yellowLowLimit: 25,
      redLowLimit: 20,
      rawValue: 102.9,
      component: SatelliteComponent.Thermostat,
      evaluatorKey: "highTemperature",
      groupKey: "TSTAT.1000"
    },{
      timestamp: new Date("2018-01-02T04:02:38.001Z"),
      satelliteId: 1000,
      redHighLimit: 101,
      yellowHighLimit: 98,
      yellowLowLimit: 25,
      redLowLimit: 20,
      rawValue: 102.9,
      component: SatelliteComponent.Thermostat,
      evaluatorKey: "highTemperature",
      groupKey: "TSTAT.1000"
    },{
      timestamp: new Date("2018-01-02T04:07:38.001Z"),
      satelliteId: 1000,
      redHighLimit: 101,
      yellowHighLimit: 98,
      yellowLowLimit: 25,
      redLowLimit: 20,
      rawValue: 102.9,
      component: SatelliteComponent.Thermostat,
      evaluatorKey: "highTemperature",
      groupKey: "TSTAT.1000"
    }])
    expect(alerts).to.have.lengthOf(0);
  });

})
