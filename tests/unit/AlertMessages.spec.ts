import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import AlertMessages from '@/components/AlertMessages.vue'

describe('AlertMessages.vue', () => {

  it('displays file name', () => {
    const fileName = 'example_file_name.txt';
    const wrapper = shallowMount(AlertMessages, {
      propsData: { fileName, messagesText: ' ' }
    });
    expect(wrapper.text()).to.include(fileName);
  });

  it('does not display alerts for single message', () => {
    const messagesText = '20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT';
    const wrapper = shallowMount(AlertMessages, {
      propsData: { messagesText }
    });
    expect(wrapper.text()).to.not.include('component');
  });

  it('displays an alert for messages that meet criteria', () => {
    const messagesText = `   
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
`;
    const wrapper = shallowMount(AlertMessages, {
      propsData: { messagesText }
    });
    expect(wrapper.text()).to.not.include(`
{
  "component": "TSTAT",
  "satelliteId": 1000,
  "severity": "RED HIGH",
  "timestamp": "2018-01-02T04:01:38.001Z"
}
`);
  });

})
