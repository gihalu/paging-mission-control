import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import FileReader from '@/components/FileReader.vue'

describe('FileReader.vue', () => {

  it('renders with custom label', () => {
    const wrapper = shallowMount(FileReader, {
    });
    expect(wrapper.text()).to.include('Upload File');
  });

  it('does not render with default input text', () => {
    const wrapper = shallowMount(FileReader, {
    });
    expect(wrapper.text()).to.not.include('No file chosen');
  });

})
