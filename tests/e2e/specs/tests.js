// https://docs.cypress.io/api/introduction/api.html

beforeEach(() => {
  cy.visit('/');
})

describe('Initial App Load', () => {
  it('Renders default message', () => {
    cy.contains('div', 'Please select a file to proceed')
  })
})

describe('File Upload', () => {
  it('Displays filename in response header', () => {
    cy.get('[data-cy="file-input"]')
      .attachFile('sample_input.txt');
    cy.get('h3')
      .contains('sample_input.txt');
  });
})

describe('Message Evaluation', () => {
  it('Displays all relevant alerts', () => {
    cy.get('[data-cy="file-input"]')
      .attachFile('sample_input.txt');
    cy.get('[data-cy="alert-messages"]')
      .contains('RED HIGH')
      .contains('RED LOW');
  });
})
