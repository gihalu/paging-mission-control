export class FileReaderService {
    public getTextFromFile(file: File): Promise<string> {
        const reader = new FileReader();
        reader.readAsText(file);

        return new Promise((resolve, reject) => {
            reader.onload = ({ target }: ProgressEvent) => {
                const { result } = target as FileReader;

                result
                    ? resolve(result as string)
                    : reject('No file results');
            };
            reader.onerror = ({ target }: ProgressEvent) => {
                const { error } = target as FileReader;

                reject(error || 'The file could not be read');
            }
        });
    }
}