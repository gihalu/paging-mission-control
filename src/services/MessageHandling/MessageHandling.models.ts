export interface alertMessage {
    satelliteId: number;
    severity: string;
    component: SatelliteComponent;
    timestamp: string;
}

export interface AlertParameters {
    violationCount: number;
    timeSpan: number;
}

export enum SatelliteComponent {
    Battery = 'BATT',
    Thermostat = 'TSTAT'
}

export interface SatelliteMessage {
    component: SatelliteComponent;
    rawValue: number;
    redHighLimit: number;
    redLowLimit: number;
    satelliteId: number;
    timestamp: Date;
    yellowHighLimit: number;
    yellowLowLimit: number;
}

export interface Violation extends SatelliteMessage {
    evaluatorKey: string;
    groupKey: string;
}

export interface SatelliteMessageEvaluator {
    alertParameters: AlertParameters;
    violation: (message: SatelliteMessage) => boolean;
    severity: string;
}

export enum StringPatterns {
    Delimiter = '|',
    NewLine = '\r\n'
}