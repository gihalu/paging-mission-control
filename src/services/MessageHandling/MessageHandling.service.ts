import { alertMessage, Violation, SatelliteComponent, SatelliteMessage, SatelliteMessageEvaluator, StringPatterns } from "./MessageHandling.models";

export class MessageHandlingService {
  private evaluators: { [key: string]: SatelliteMessageEvaluator } = {
    highTemperature: {
      alertParameters: {
        violationCount: 3,
        timeSpan: 5 * 60 * 1000 // 5 minutes
      },
      violation: ({ component, rawValue, redHighLimit }: SatelliteMessage): boolean => {
        return component == SatelliteComponent.Thermostat ? rawValue > redHighLimit : false;
      },
      severity: 'RED HIGH'
    },
    lowBattery: {
      alertParameters: {
        violationCount: 3,
        timeSpan: 5 * 60 * 1000 // 5 minutes
      },
      violation: ({ component, rawValue, redLowLimit }: SatelliteMessage): boolean => {
        return component == SatelliteComponent.Battery ? rawValue < redLowLimit : false;
      },
      severity: 'RED LOW'
    }
  }

  private convertTimestamp(timestamp: string): Date {
    const [dateString, timeString] = timestamp.split(' ');
    const yyyy = dateString.slice(0, 4);
    const mm = dateString.slice(4, 6);
    const dd = dateString.slice(6, 8);
    return new Date(`${yyyy}-${mm}-${dd}T${timeString}`);
  }

  public getAlertMessages(violations: Violation[]): alertMessage[] {
    if (!violations || !violations.length) return [];

    const alerts: alertMessage[] = [];
    const groupKeys = [...new Set(violations.map(message => message.groupKey))];

    groupKeys.forEach(groupKey => {
      const messageGroup: Violation[] = violations.filter(message => message.groupKey == groupKey);
      const { alertParameters, severity } = this.evaluators[messageGroup[0].evaluatorKey];
      const subtrahend = alertParameters.violationCount - 1;

      for (const [index, message] of messageGroup.entries()) {
        if (index < subtrahend) continue;

        const comparisonMessage = messageGroup[index - subtrahend];
        const withinTimeSpan = message.timestamp.valueOf() - comparisonMessage.timestamp.valueOf() <= alertParameters.timeSpan;
        if (withinTimeSpan) {
          const alertMessage: alertMessage = {
            component: message.component,
            satelliteId: message.satelliteId,
            severity,
            timestamp: comparisonMessage.timestamp.toISOString(),
          }
          alerts.push(alertMessage);
          break;
        }
      }
    });
    
    return alerts;
  }

  public getViolations(messages: SatelliteMessage[]): Violation[] {
    return Object.entries(this.evaluators).map(([key, evaluator]) => {
      const violations: Violation[] = messages
        .filter(evaluator.violation)
        .map(message => { return {
          ...message,
          evaluatorKey: key,
          groupKey: `${message.component}.${message.satelliteId}`
        }});
      return violations;
    }).flat();
  }

  public getMessagesFromText(text: string): SatelliteMessage[] {
    return text
      .split(StringPatterns.NewLine)
      .map((row: string) => {
        const [
          timestamp,
          satelliteId,
          redHighLimit,
          yellowHighLimit,
          yellowLowLimit,
          redLowLimit,
          rawValue,
          component
        ] = row.split(StringPatterns.Delimiter);

        return {
          timestamp: this.convertTimestamp(timestamp),
          satelliteId: Number(satelliteId),
          redHighLimit: Number(redHighLimit),
          yellowHighLimit: Number(yellowHighLimit),
          yellowLowLimit: Number(yellowLowLimit),
          redLowLimit: Number(redLowLimit),
          rawValue: Number(rawValue),
          component: component as SatelliteComponent
        };
      });

  }
}